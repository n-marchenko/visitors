﻿using System;

namespace Visitors.Core
{
    public static class DateTimeProvider
    {
        public static DateTimeOffset GetCurrentDate()
        {
            return DateTimeOffset.Now; 
        }
    }
}
