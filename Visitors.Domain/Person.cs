﻿using System.Collections.Generic;

namespace Visitors.Domain
{
    /// <summary>
    /// представляет человека к которому приходят
    /// </summary>
    public class Person : Human
    {
        public ICollection<Visit> Visits { get; set; }

        public Person()
        {
            Visits = new List<Visit>();
        }
    }
}
