﻿using Microsoft.EntityFrameworkCore;
using Visitors.Data.Extensions;
using Visitors.Domain;

namespace Visitors.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options):base(options)
        {
            
        }
        public DbSet<Visitor> Visitors { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Visit> Visits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ConfigureVisitedModel();
            modelBuilder.ConfigureVisitorModel();
            modelBuilder.ConfigureVisitModel();
        }
    }
}
