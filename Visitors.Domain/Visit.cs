﻿using System;
using Visitors.Domain.Abstract;

namespace Visitors.Domain
{
    /// <summary>
    /// представляет посещение
    /// </summary>
    public class Visit : Entity
    {
        public DateTimeOffset VisitTime { get; set; }
        public int Floor { get; set; }
        public string Room{ get; set; }

        #region Relationship
        public Guid VisitedPersonId { get; set; }
        public Person VisitedPerson { get; set; }
        public Guid VisitorPersonId { get; set; }
        public Visitor VisitorPerson { get; set; }
        #endregion
    }
}
